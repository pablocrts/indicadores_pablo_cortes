

const carpetas = 'datos/';
const fs = require('fs'); 
var archivos = [];


function promedios(lector){
     
    const leerOpcion = (lectura)=>{
        return new Promise((resolve,reject)=>{
            //console.log('Que desea promediar');
            lectura.question('Indicador a promediar: 1. Dolar 2.Euro 3.Tasa de desempleo ', opcion =>{
                resolve(opcion);
            });
        });
    }
    const leerDirectorio =()=>{
        archivos=[];
        return new Promise((resolve,reject)=>{
            fs.readdirSync(carpetas).forEach(file => {
                let fileName=`datos/${file}`;
                let content=fs.readFileSync(fileName,"utf8");
                content=JSON.parse(content);
                archivos.push(content[0]);
               
            });
        })
    }

    const calcularPromedio=(opcion)=>{
        let sumaDolar=0,sumaEuro=0,sumaTasa=0;
        let promDolar=0,promEuro=0,promTasa=0;
        for (let i = 0; i < archivos.length; i++) {
            sumaDolar += Number(archivos[i].dolar.valor);
            sumaEuro += Number(archivos[i].euro.valor);
            sumaTasa += Number(archivos[i].euro.valor);
        }
        fechaInicial = (archivos[0].fecha);
        fechaFinal = (archivos[archivos.length-1].fecha);
        promDolar=sumaDolar/archivos.length;
        promEuro=sumaEuro/archivos.length;
        promTasa=sumaTasa/archivos.length;

        if(opcion=='1'){
            console.log(fechaInicial+' a '+fechaFinal);
            console.log('Promedio del dolar: '+promDolar);   
        }
        if(opcion=='2'){
            console.log(fechaInicial+' a '+fechaFinal);
            console.log('Promedio del euro: '+promEuro);
        }
        if(opcion=='3'){
            console.log(fechaInicial+' a '+fechaFinal);
            console.log('promedio de la tasa de desempleo: '+promTasa);
        }
        lector.close();
        
    }

    leerDirectorio()
    .then(leerOpcion(lector)
    .then( (opcion)=>{ (calcularPromedio(opcion))})
    .catch(console.error));

}
   
module.exports=promedios;
//calcularPromedio();
